begin_problem(Sokrates1).

list_of_descriptions.
name({*Sokrates*}).
author({*Christoph Weidenbach*}).
status(unsatisfiable).
description({* Sokrates is mortal and since all humans are mortal, he is mortal too. *}).
end_of_list.

list_of_symbols.
  functions[P1,P2,P3,H1,H2].
  predicates[Pigeon,Hole,InHole].
end_of_list.

list_of_formulae(axioms).
	formula(not(equal(P1,P2))).
	formula(not(equal(P2,P3))).
	formula(not(equal(P1,P3))).
	formula(not(equal(P1,H1))).
	formula(not(equal(P2,H1))).
	formula(not(equal(P3,H1))).
	formula(not(equal(P1,H2))).
	formula(not(equal(P2,H2))).
	formula(not(equal(P3,H2))).
	formula(not(equal(H1,H2))).
	
	formula(forall([x],equiv(Pigeon(x),or(equal(x,P1),or(equal(x,P2),equal(x,P3)))))).
	formula(forall([x],equiv(Hole(x),or(equal(x,H1),equal(x,H2))))).

	formula(forall([p,h],implies(InHole(p,h),and(Pigeon(p),Hole(h))))).
	formula(forall([h,p1,p2],implies(and(InHole(p1,h),InHole(p2,h)),equal(p1,p2)))).
end_of_list.

list_of_formulae(conjectures).
	formula(exists([p],and(Pigeon(p),forall([h],implies(Hole(h),not(InHole(p,h))))))).
end_of_list.

end_problem.

// 2 pigeons, 1 hole:
1[0:Inp] || equal(H1,P2)** -> .
2[0:Inp] || equal(P2,P1)** -> .
3[0:Inp] Hole(U) ||  -> equal(U,H1)*.
5[0:Inp] || equal(U,P2) -> Pigeon(U)*.
6[0:Inp] || equal(U,P1) -> Pigeon(U)*.
7[0:Inp] Pigeon(U) ||  -> Hole(skf1(V))*.
8[0:Inp] Pigeon(U) ||  -> InHole(U,skf1(U))*.
10[0:Inp] || InHole(U,V)*+ InHole(W,V)* -> equal(U,W)*.
12[0:EmS:7.0,6.1] || equal(U,P1)* -> Hole(skf1(V))*.
14[0:AED:1.0,12.0] ||  -> Hole(skf1(U))*.
16[0:EmS:3.0,14.0] ||  -> equal(skf1(U),H1)**.
18[0:Rew:16.0,8.1] Pigeon(U) ||  -> InHole(U,H1)*.
23[0:Res:18.1,10.0] Pigeon(U) || InHole(V,H1)*+ -> equal(U,V)*.
24[0:Res:18.1,23.1] Pigeon(U) Pigeon(V) ||  -> equal(V,U)*.
25[0:EmS:24.0,6.1] Pigeon(U) || equal(V,P1)* -> equal(U,V)*.
28[0:EmS:25.0,5.1] || equal(U,P1)*+ equal(V,P2)* -> equal(V,U)*.
32[0:EqR:28.0] || equal(U,P2)* -> equal(U,P1).
34[0:EqR:32.0] ||  -> equal(P2,P1)**.
35[0:MRR:34.0,2.0] ||  -> .

// 3 pigeons, 2 holes:
1[0:Inp] || equal(H2,H1)** -> .
3[0:Inp] || equal(H2,P2)** -> .
4[0:Inp] || equal(H2,P1)** -> .
6[0:Inp] || equal(H1,P2)** -> .
7[0:Inp] || equal(H1,P1)** -> .
8[0:Inp] || equal(P3,P1)** -> .
9[0:Inp] || equal(P3,P2)** -> .
10[0:Inp] || equal(P2,P1)** -> .
12[0:Inp] || InHole(U,V)* -> Pigeon(U).
15[0:Inp] || equal(U,P3) -> Pigeon(U)*.
16[0:Inp] || equal(U,P2) -> Pigeon(U)*.
17[0:Inp] || equal(U,P1) -> Pigeon(U)*.
18[0:Inp] Pigeon(U) ||  -> Hole(skf1(V))*.
19[0:Inp] Pigeon(U) ||  -> InHole(U,skf1(U))*.
20[0:Inp] Hole(U) ||  -> equal(U,H1) equal(U,H2)*.
21[0:Inp] || InHole(U,V)*+ InHole(W,V)* -> equal(U,W)*.
23[0:EmS:18.0,17.1] || equal(U,P1)* -> Hole(skf1(V))*.
26[0:AED:1.0,23.0] ||  -> Hole(skf1(U))*.
31[0:EmS:20.0,26.0] ||  -> equal(skf1(U),H1) equal(skf1(U),H2)**.
35[0:SpR:31.1,26.0] ||  -> equal(skf1(U),H1)** Hole(H2).
36[0:SpR:31.1,19.1] Pigeon(U) ||  -> equal(skf1(U),H1) InHole(U,H2)*.
38[1:Spt:35.0] ||  -> equal(skf1(U),H1)**.
40[1:Rew:38.0,19.1] Pigeon(U) ||  -> InHole(U,H1)*.
45[1:Res:40.1,21.0] Pigeon(U) || InHole(V,H1)*+ -> equal(U,V)*.
46[1:Res:40.1,45.1] Pigeon(U) Pigeon(V) ||  -> equal(V,U)*.
47[1:EmS:46.0,17.1] Pigeon(U) || equal(V,P1)* -> equal(U,V)*.
51[1:EmS:47.0,16.1] || equal(U,P1)*+ equal(V,P2)* -> equal(V,U)*.
63[1:EqR:51.0] || equal(U,P2)* -> equal(U,P1).
64[1:EqR:63.0] ||  -> equal(P2,P1)**.
65[1:MRR:64.0,10.0] ||  -> .
66[1:Spt:65.0,35.1] ||  -> Hole(H2)*.
68[0:Res:19.1,21.0] Pigeon(U) || InHole(V,skf1(U))* -> equal(U,V).
76[0:Res:36.2,21.0] Pigeon(U) || InHole(V,H2)*+ -> equal(skf1(U),H1)** equal(U,V)*.
81[0:Res:36.2,76.1] Pigeon(U) Pigeon(V) ||  -> equal(skf1(U),H1)** equal(skf1(V),H1)** equal(V,U)*.
85[0:SpR:81.2,19.1] Pigeon(U) Pigeon(V) Pigeon(U) ||  -> equal(skf1(V),H1)** equal(V,U)* InHole(U,H1)*.
106[0:SpL:81.3,68.1] Pigeon(U) Pigeon(V) Pigeon(V) || InHole(W,H1)* -> equal(skf1(U),H1)** equal(V,U)* equal(V,W)*.
109[0:Obv:85.0] Pigeon(U) Pigeon(V) ||  -> equal(skf1(U),H1)** equal(U,V)* InHole(V,H1)*.
111[0:Obv:106.1] Pigeon(U) Pigeon(V) || InHole(W,H1)*+ -> equal(skf1(U),H1)** equal(V,U)* equal(V,W)*.
121[0:SpR:109.2,19.1] Pigeon(U) Pigeon(V) Pigeon(U) ||  -> equal(U,V)* InHole(V,H1)* InHole(U,H1)*.
145[0:Obv:121.0] Pigeon(U) Pigeon(V) ||  -> equal(V,U)* InHole(U,H1)* InHole(V,H1)*.
157[0:SpR:145.2,26.0] Pigeon(U) Pigeon(skf1(V)) ||  -> InHole(U,H1)* InHole(skf1(V),H1)* Hole(U).
187[0:SoR:157.1,17.1] Pigeon(U) || equal(skf1(V),P1)+ -> InHole(U,H1)* InHole(skf1(V),H1)* Hole(U).
188[0:SoR:157.1,16.1] Pigeon(U) || equal(skf1(V),P2)+ -> InHole(U,H1)* InHole(skf1(V),H1)* Hole(U).
189[0:SoR:157.1,15.1] Pigeon(U) || equal(skf1(V),P3)+ -> InHole(U,H1)* InHole(skf1(V),H1)* Hole(U).
196[2:Spt:187.0,187.2,187.4] Pigeon(U) ||  -> InHole(U,H1)* Hole(U).
197[2:Res:196.1,21.0] Pigeon(U) || InHole(V,H1)*+ -> Hole(U)* equal(U,V)*.
203[0:Res:109.4,111.2] Pigeon(U) Pigeon(V) Pigeon(W) Pigeon(X) ||  -> equal(skf1(U),H1)** equal(U,V)* equal(skf1(W),H1)** equal(X,W)* equal(X,V)*.
205[2:Res:196.1,197.1] Pigeon(U) Pigeon(V) ||  -> Hole(U)* Hole(V)* equal(V,U)*.
209[2:EmS:20.0,205.2] Pigeon(U) Pigeon(V) ||  -> equal(V,H1) equal(V,H2)* equal(U,V)* Hole(U)*.
264[2:SpL:209.3,4.0] Pigeon(U) Pigeon(V) || equal(V,P1) -> equal(V,H1)* equal(U,V)* Hole(U)*.
273[2:MRR:264.1,17.1] Pigeon(U) || equal(V,P1)+ -> equal(V,H1)* equal(U,V)* Hole(U)*.
292[2:EqR:273.1] Pigeon(U) ||  -> equal(H1,P1) equal(U,P1) Hole(U)*.
293[2:MRR:292.1,7.0] Pigeon(U) ||  -> equal(U,P1) Hole(U)*.
294[2:EmS:20.0,293.2] Pigeon(U) ||  -> equal(U,H1) equal(U,H2)* equal(U,P1).
296[2:EmS:294.0,16.1] || equal(U,P2) -> equal(U,H1) equal(U,H2)* equal(U,P1).
321[2:SpL:296.2,3.0] || equal(U,P2) equal(U,P2) -> equal(U,H1)* equal(U,P1).
327[2:Obv:321.0] || equal(U,P2) -> equal(U,H1)* equal(U,P1).
367[2:SpL:327.1,6.0] || equal(U,P2)* equal(U,P2)* -> equal(U,P1).
374[2:Obv:367.0] || equal(U,P2)* -> equal(U,P1).
376[2:EqR:374.0] ||  -> equal(P2,P1)**.
377[2:MRR:376.0,10.0] ||  -> .
378[2:Spt:377.0,187.1,187.3] || equal(skf1(U),P1) -> InHole(skf1(U),H1)*.
382[2:SpR:109.3,378.1] Pigeon(skf1(U)) Pigeon(V) || equal(skf1(U),P1) -> equal(skf1(skf1(U)),H1)** InHole(V,H1)* InHole(V,H1)*.
402[2:Obv:382.4] Pigeon(skf1(U)) Pigeon(V) || equal(skf1(U),P1) -> equal(skf1(skf1(U)),H1)** InHole(V,H1)*.
403[2:MRR:402.0,17.1] Pigeon(U) || equal(skf1(V),P1)+ -> equal(skf1(skf1(V)),H1)** InHole(U,H1)*.
421[3:Spt:403.0,403.3] Pigeon(U) ||  -> InHole(U,H1)*.
422[3:Res:421.1,21.0] Pigeon(U) || InHole(V,H1)*+ -> equal(U,V)*.
430[3:Res:421.1,422.1] Pigeon(U) Pigeon(V) ||  -> equal(V,U)*.
431[3:EmS:430.0,17.1] Pigeon(U) || equal(V,P1)* -> equal(U,V)*.
435[3:EmS:431.0,16.1] || equal(U,P1)*+ equal(V,P2)* -> equal(V,U)*.
453[3:EqR:435.0] || equal(U,P2)* -> equal(U,P1).
465[3:EqR:453.0] ||  -> equal(P2,P1)**.
466[3:MRR:465.0,10.0] ||  -> .
467[3:Spt:466.0,403.1,403.2] || equal(skf1(U),P1) -> equal(skf1(skf1(U)),H1)**.
631[4:Spt:188.0,188.2,188.4] Pigeon(U) ||  -> InHole(U,H1)* Hole(U).
632[4:Res:631.1,21.0] Pigeon(U) || InHole(V,H1)*+ -> Hole(U)* equal(U,V)*.
636[4:Res:631.1,632.1] Pigeon(U) Pigeon(V) ||  -> Hole(U)* Hole(V)* equal(V,U)*.
641[4:EmS:20.0,636.2] Pigeon(U) Pigeon(V) ||  -> equal(V,H1) equal(V,H2)* equal(U,V)* Hole(U)*.
735[4:SpL:641.3,4.0] Pigeon(U) Pigeon(V) || equal(V,P1) -> equal(V,H1)* equal(U,V)* Hole(U)*.
748[4:MRR:735.1,17.1] Pigeon(U) || equal(V,P1)+ -> equal(V,H1)* equal(U,V)* Hole(U)*.
760[4:EqR:748.1] Pigeon(U) ||  -> equal(H1,P1) equal(U,P1) Hole(U)*.
761[4:MRR:760.1,7.0] Pigeon(U) ||  -> equal(U,P1) Hole(U)*.
767[4:EmS:20.0,761.2] Pigeon(U) ||  -> equal(U,H1) equal(U,H2)* equal(U,P1).
769[4:EmS:767.0,16.1] || equal(U,P2) -> equal(U,H1) equal(U,H2)* equal(U,P1).
799[4:SpL:769.2,3.0] || equal(U,P2) equal(U,P2) -> equal(U,H1)* equal(U,P1).
805[4:Obv:799.0] || equal(U,P2) -> equal(U,H1)* equal(U,P1).
858[4:SpL:805.1,6.0] || equal(U,P2)* equal(U,P2)* -> equal(U,P1).
865[4:Obv:858.0] || equal(U,P2)* -> equal(U,P1).
867[4:EqR:865.0] ||  -> equal(P2,P1)**.
868[4:MRR:867.0,10.0] ||  -> .
869[4:Spt:868.0,188.1,188.3] || equal(skf1(U),P2) -> InHole(skf1(U),H1)*.
872[4:SpR:109.3,869.1] Pigeon(skf1(U)) Pigeon(V) || equal(skf1(U),P2) -> equal(skf1(skf1(U)),H1)** InHole(V,H1)* InHole(V,H1)*.
894[4:Obv:872.4] Pigeon(skf1(U)) Pigeon(V) || equal(skf1(U),P2) -> equal(skf1(skf1(U)),H1)** InHole(V,H1)*.
895[4:MRR:894.0,16.1] Pigeon(U) || equal(skf1(V),P2)+ -> equal(skf1(skf1(V)),H1)** InHole(U,H1)*.
911[5:Spt:895.0,895.3] Pigeon(U) ||  -> InHole(U,H1)*.
912[5:Res:911.1,21.0] Pigeon(U) || InHole(V,H1)*+ -> equal(U,V)*.
931[5:Res:911.1,912.1] Pigeon(U) Pigeon(V) ||  -> equal(V,U)*.
932[5:EmS:931.0,17.1] Pigeon(U) || equal(V,P1)* -> equal(U,V)*.
936[5:EmS:932.0,16.1] || equal(U,P1)*+ equal(V,P2)* -> equal(V,U)*.
962[5:EqR:936.0] || equal(U,P2)* -> equal(U,P1).
963[5:EqR:962.0] ||  -> equal(P2,P1)**.
964[5:MRR:963.0,10.0] ||  -> .
965[5:Spt:964.0,895.1,895.2] || equal(skf1(U),P2) -> equal(skf1(skf1(U)),H1)**.
1210[6:Spt:189.0,189.2,189.4] Pigeon(U) ||  -> InHole(U,H1)* Hole(U).
1211[6:Res:1210.1,21.0] Pigeon(U) || InHole(V,H1)*+ -> Hole(U)* equal(U,V)*.
1215[6:Res:1210.1,1211.1] Pigeon(U) Pigeon(V) ||  -> Hole(U)* Hole(V)* equal(V,U)*.
1221[6:EmS:20.0,1215.2] Pigeon(U) Pigeon(V) ||  -> equal(V,H1) equal(V,H2)* equal(U,V)* Hole(U)*.
1356[6:SpL:1221.3,4.0] Pigeon(U) Pigeon(V) || equal(V,P1) -> equal(V,H1)* equal(U,V)* Hole(U)*.
1373[6:MRR:1356.1,17.1] Pigeon(U) || equal(V,P1)+ -> equal(V,H1)* equal(U,V)* Hole(U)*.
1387[6:EqR:1373.1] Pigeon(U) ||  -> equal(H1,P1) equal(U,P1) Hole(U)*.
1388[6:MRR:1387.1,7.0] Pigeon(U) ||  -> equal(U,P1) Hole(U)*.
1398[6:EmS:20.0,1388.2] Pigeon(U) ||  -> equal(U,H1) equal(U,H2)* equal(U,P1).
1400[6:EmS:1398.0,16.1] || equal(U,P2) -> equal(U,H1) equal(U,H2)* equal(U,P1).
1435[6:SpL:1400.2,3.0] || equal(U,P2) equal(U,P2) -> equal(U,H1)* equal(U,P1).
1441[6:Obv:1435.0] || equal(U,P2) -> equal(U,H1)* equal(U,P1).
1500[6:SpL:1441.1,6.0] || equal(U,P2)* equal(U,P2)* -> equal(U,P1).
1507[6:Obv:1500.0] || equal(U,P2)* -> equal(U,P1).
1511[6:EqR:1507.0] ||  -> equal(P2,P1)**.
1512[6:MRR:1511.0,10.0] ||  -> .
1513[6:Spt:1512.0,189.1,189.3] || equal(skf1(U),P3) -> InHole(skf1(U),H1)*.
1516[6:SpR:109.3,1513.1] Pigeon(skf1(U)) Pigeon(V) || equal(skf1(U),P3) -> equal(skf1(skf1(U)),H1)** InHole(V,H1)* InHole(V,H1)*.
1540[6:Obv:1516.4] Pigeon(skf1(U)) Pigeon(V) || equal(skf1(U),P3) -> equal(skf1(skf1(U)),H1)** InHole(V,H1)*.
1541[6:MRR:1540.0,15.1] Pigeon(U) || equal(skf1(V),P3)+ -> equal(skf1(skf1(V)),H1)** InHole(U,H1)*.
1558[7:Spt:1541.0,1541.3] Pigeon(U) ||  -> InHole(U,H1)*.
1559[7:Res:1558.1,21.0] Pigeon(U) || InHole(V,H1)*+ -> equal(U,V)*.
1580[7:Res:1558.1,1559.1] Pigeon(U) Pigeon(V) ||  -> equal(V,U)*.
1581[7:EmS:1580.0,17.1] Pigeon(U) || equal(V,P1)* -> equal(U,V)*.
1585[7:EmS:1581.0,16.1] || equal(U,P1)*+ equal(V,P2)* -> equal(V,U)*.
1617[7:EqR:1585.0] || equal(U,P2)* -> equal(U,P1).
1618[7:EqR:1617.0] ||  -> equal(P2,P1)**.
1619[7:MRR:1618.0,10.0] ||  -> .
1620[7:Spt:1619.0,1541.1,1541.2] || equal(skf1(U),P3) -> equal(skf1(skf1(U)),H1)**.
2259[0:Fac:203.4,203.6] Pigeon(U) Pigeon(V) Pigeon(U) Pigeon(W) ||  -> equal(U,V)* equal(skf1(U),H1)** equal(W,U)* equal(W,V)*.
2554[0:SpL:203.8,8.0] Pigeon(U) Pigeon(P3) Pigeon(V) Pigeon(W) || equal(W,P1)* -> equal(skf1(U),H1)** equal(U,P3) equal(skf1(V),H1)** equal(W,V)*.
2575[0:Obv:2259.0] Pigeon(U) Pigeon(V) Pigeon(W) ||  -> equal(V,U)* equal(skf1(V),H1)** equal(W,V)* equal(W,U)*.
2599[0:MRR:2554.3,17.1] Pigeon(U) Pigeon(P3) Pigeon(V) || equal(W,P1)* -> equal(skf1(U),H1)** equal(U,P3) equal(skf1(V),H1)** equal(W,V)*.
3025[0:SpL:2575.5,8.0] Pigeon(U) Pigeon(P3) Pigeon(V) || equal(V,P1)* -> equal(P3,U)* equal(skf1(P3),H1)** equal(V,U)*.
3111[0:MRR:3025.2,17.1] Pigeon(U) Pigeon(P3) || equal(V,P1)* -> equal(P3,U)* equal(skf1(P3),H1)** equal(V,U)*.
3280[0:SoR:3111.1,15.1] Pigeon(U) || equal(V,P1)* equal(P3,P3) -> equal(P3,U)* equal(skf1(P3),H1)** equal(V,U)*.
3281[0:Obv:3280.2] Pigeon(U) || equal(V,P1)*+ -> equal(P3,U)* equal(skf1(P3),H1)** equal(V,U)*.
3283[8:Spt:3281.3] ||  -> equal(skf1(P3),H1)**.
3284[8:SpR:3283.0,19.1] Pigeon(P3) ||  -> InHole(P3,H1)*.
3329[8:SoR:3284.0,15.1] || equal(P3,P3) -> InHole(P3,H1)*.
3330[8:Obv:3329.0] ||  -> InHole(P3,H1)*.
3353[8:Res:3330.0,21.0] || InHole(U,H1)* -> equal(P3,U).
3354[8:Res:3330.0,12.0] ||  -> Pigeon(P3)*.
3358[8:MRR:2599.1,3354.0] Pigeon(U) Pigeon(V) || equal(W,P1)*+ -> equal(skf1(U),H1)** equal(U,P3) equal(skf1(V),H1)** equal(W,V)*.
3634[9:Spt:3358.1,3358.2,3358.5,3358.6] Pigeon(U) || equal(V,P1)*+ -> equal(skf1(U),H1)** equal(V,U)*.
3635[9:EqR:3634.1] Pigeon(U) ||  -> equal(skf1(U),H1)** equal(P1,U).
3644[9:SpR:3635.1,19.1] Pigeon(U) Pigeon(U) ||  -> equal(P1,U) InHole(U,H1)*.
3664[9:Obv:3644.0] Pigeon(U) ||  -> equal(P1,U) InHole(U,H1)*.
3680[9:Res:3664.2,3353.0] Pigeon(U) ||  -> equal(P1,U) equal(P3,U)*.
3685[9:EmS:3680.0,16.1] || equal(U,P2) -> equal(P1,U) equal(P3,U)*.
3798[9:SpL:3685.2,9.0] || equal(U,P2)* equal(U,P2)* -> equal(P1,U).
3821[9:Obv:3798.0] || equal(U,P2)* -> equal(P1,U).
3872[9:EqR:3821.0] ||  -> equal(P2,P1)**.
3881[9:MRR:3872.0,10.0] ||  -> .
3882[9:Spt:3881.0,3358.0,3358.3,3358.4] Pigeon(U) ||  -> equal(skf1(U),H1)** equal(U,P3).
3958[9:SpR:3882.1,19.1] Pigeon(U) Pigeon(U) ||  -> equal(U,P3) InHole(U,H1)*.
3978[9:Obv:3958.0] Pigeon(U) ||  -> equal(U,P3) InHole(U,H1)*.
3979[9:MRR:3978.2,3353.0] Pigeon(U) ||  -> equal(U,P3)*.
3993[9:EmS:3979.0,17.1] || equal(U,P1) -> equal(U,P3)*.
4043[9:SpL:3993.1,8.0] || equal(U,P1)* equal(U,P1)* -> .
4048[9:Obv:4043.0] || equal(U,P1)* -> .
4049[9:AED:1.0,4048.0] ||  -> .
4063[8:Spt:4049.0,3281.3,3283.0] || equal(skf1(P3),H1)** -> .
4064[8:Spt:4049.0,3281.0,3281.1,3281.2,3281.4] Pigeon(U) || equal(V,P1)* -> equal(P3,U)* equal(V,U)*.
4069[8:EmS:4064.0,16.1] || equal(U,P1)*+ equal(V,P2) -> equal(P3,V)* equal(U,V)*.
4590[8:EqR:4069.0] || equal(U,P2) -> equal(P3,U)* equal(P1,U).
4631[8:SpL:4590.1,9.0] || equal(U,P2)* equal(U,P2)* -> equal(P1,U).
4655[8:Obv:4631.0] || equal(U,P2)* -> equal(P1,U).
4664[8:EqR:4655.0] ||  -> equal(P2,P1)**.
4673[8:MRR:4664.0,10.0] ||  -> .