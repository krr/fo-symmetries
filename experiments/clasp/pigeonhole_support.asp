% Theory
{inhole(P,H)} :- pigeon(P), hole(H).
:- pigeon(P), 2<={inhole(P,H):hole(H)}.
:- pigeon(P), {inhole(P,H):hole(H)}<=0.
:- inhole(P1,H), inhole(P2,H), P1!=P2.

% Data
pigeon(1..12).
hole(1..11).

% Display
#show inhole/2.
