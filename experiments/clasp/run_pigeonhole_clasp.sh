#!/bin/sh
cd ~/workspace/fo-symmetries/experiments/clasp
echo "grounding"
gringo-4.5.4/gringo pigeonhole_disjunctive_redundant.asp > pigeonhole.grounded
echo "breaking"
cat pigeonhole.grounded | sbass/sbass --show > pigeonhole.broken
#cat pigeonhole.grounded > pigeonhole.broken
echo "solving"
#cat pigeonhole.broken | clasp-3.1.4/clasp-3.1.4-x86_64-linux
