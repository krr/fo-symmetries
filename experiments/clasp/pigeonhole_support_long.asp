% Theory
{inhole(P,H)} :- pigeon(P), hole(H).
:- inhole(P1,H), inhole(P2,H), P1!=P2.
:- pigeon(P), 2<={inhole(P,H):hole(H)}.
:- pigeon(P)
, not inhole(P,1)
, not inhole(P,2)
, not inhole(P,3)
, not inhole(P,4)
, not inhole(P,5)
, not inhole(P,6)
, not inhole(P,7)
, not inhole(P,8)
, not inhole(P,9)
, not inhole(P,10)
, not inhole(P,11)
.

% Data
pigeon(1..12).
hole(1..11).

% Display
#show inhole/2.
