In this section, we give two local domain symmetry detection algorithms for model expansion problems.
The first detects generators of a local domain symmetry group, the second derives interchangeable subdomains.
Both approaches work on a first-order level, avoiding the need to \emph{ground} the model expansion problem to a propositional counterpart.
%Given a model expansion problem $MX(\theory,\structin)$, the first approach detects local domain symmetry generators $\locdomsym{\pi}{A}$, the second derives subdomain interchangeability groups $\locdomintch{\delta}{A}$.
Both algorithms are based on Theorem~\ref{thm:locdomsymconditionmx}, which conditions argument position set $A$ to be connectively closed under decomposition theory $\structinstar$.
To find such $A$, one simply constructs a partition of $\theorystar$'s argument positions.
Using a disjoint-set data structure\footnote{\label{note:disjointset}\url{en.wikipedia.org/wiki/Disjoint-set_data_structure}},
%Using a disjoint-set data structure \cite{disjoint_set},
the computational cost to find $A$ is linear in the size of $\theory$.
In the following subsections, we assume a set of argument positions $A$ satisfying the connectedness condition is available, leaving only the concern of finding an appropriate domain permutation $\pi$ (Section \ref{sec:locdomsymdetect}) or interchangeable subdomain $\delta$ (Section \ref{sec:subd_intch_detection}).

\subsection{Local domain symmetry detection}
\label{sec:locdomsymdetect}
%Given a model expansion problem $MX(\theory,\structin)$ and a set of argument positions $A$, 
%the challenge is to derive domain permutations $\pi$ such that $\locdomsym{\pi}{A}$ preserves $\structinstar$ -- the second condition of Theorem~\ref{thm:locdomsymconditionmx}.
% 
Our approach follows other symmetry detection techniques~\cite{2002Aloul,drtiwa11a} by converting the symmetry detection problem to a \emph{graph automorphism} detection problem.
An \emph{automorphism} of a graph is a permutation $\tau$ of its vertices such that each vertex pair $(v,u)$ forms an edge iff $(\tau(v),\tau(u))$ forms an edge. If the graph is colored, then each vertex $v$ must have the same color as $\tau(v)$.

This existing work encodes a propositional theory into a graph, which we call the \emph{detection graph}.
If the detection graph is well-constructed, its automorphism group corresponds to a symmetry group of the propositional theory.
Tools such as \saucy{}~\cite{saucy} then are employed to derive generators for the detection graph's automorphism group, which in turn are converted to symmetry generators for the propositional theory.

Our approach differs by not encoding a propositional theory into the detection graph, but an input structure and a set of argument positions, as these are all we need to detect local domain symmetry.
% 
Formally, given a structure $\struct$ and an argument position set $A$, we construct an undirected colored graph whose automorphisms correspond to domain permutations $\pi$ such that $\locdomsym{\pi}{A}(\struct)=\struct$ -- satisfying the second condition of Theorem~\ref{thm:locdomsymconditionmx}.
% (see Theorem \ref{thm:domainpermutationgraph}).
% We refer to this graph as the \emph{domain permutation graph} $\dpg{\struct}{A}$.

\begin{definition}[Domain permutation graph]
Let $\struct$ be a $\voc$-structure with domain $\domain$ and $A$ a set of argument positions.
The \emph{domain permutation graph} $\dpg{\struct}{A}$ for $\struct$ and $A$ is an undirected colored graph with labeled vertices $V$, edges $E$ and color function $C$ that satisfies the following requirements:

$V$ is partitioned into three subsets: $DE$ (domain element vertices), $AP$ (argument position vertices) and $IT$ (interpretation tuple vertices).
$DE$ contains a vertex labeled $d$ for each $d \in \domain$.
%\bart{Ook: er zijn labels en colors. Zijn de labels belangrijk behalve om terug te verwijzen (op dit punt van de text dacht ik dat uw labels colors waren}
%\jo{nope, labels zijn id's om aan te geven welke "betekenis" de node heeft. Is idd verwarrend.}
%\jo{Heb footnote toegevoegd.}
$AP$ contains $k+1$ vertices labeled $\{\argnode{d}{i} \mid i \in [0..k]\}$ for each $d \in \domain$, with $k$ the maximum arity of symbols in $\voc$.
$IT$ contains a vertex labeled $S(\bar{d})$ for each tuple $\bar{d} \in S^{\struct}$ with $S^\struct \in \struct$.

$E$ consists only of edges between $DE$ and $AP$, and between $AP$ and $IT$.
An $AP$ vertex labeled $\argnode{d}{i}$ is connected to a $DE$ vertex $e$ iff $d=e$.
An $IT$ vertex labeled $S(\ldots,d_i,\ldots)$ is connected to an $AP$ vertex $\argnode{e}{j}$ iff $d=e$, $i=j$ and $\argpos{S}{i} \in A$.

Vertices from different partitions have different colors.
All $DE$ vertices have the same color.
Two $AP$ vertices labeled $\argnode{d}{i}$ and $\argnode{e}{j}$ have the same color iff $i=j$.
Two $IT$ vertices labeled $S(d_1,\ldots,d_n)$ and $R(e_1,\ldots,e_n)$ have the same color iff $S=R$ and $d_i=e_i$ for all $i$ such that $\argpos{S}{i} \not \in A$.
\end{definition}

%The domain permutation graph $\dpg{\struct}{A}$ has its vertices partitioned into three layers:
%\begin{compactitem}
%\item Domain element layer (DE)
%\item Argument position layer (AP)
%\item Interpretation tuple layer (IT)
%\end{compactitem}
%Each layer's vertices are colored with a set of colors different from those of the other layers.
%No edges exist between two nodes of the same layer, and no edges exist between DE and IT; the only edges are between DE and AP and between AP and IT.

%DE contains a vertex labeled $d$ for each domain element $d \in \domain$. All DE vertices have the same color.
%For each $d \in \domain$, AP contains $k+1$ vertices labeled $\{\argnode{d}{i} \mid i \in [0..k]\}$, with $k$ the maximum arity of symbols interpreted by $\struct$.
%Two AP vertices labeled $\argnode{d}{i}$ and $\argnode{e}{j}$ have the same color iff $i=j$.
%Each AP vertex $\argnode{d}{i}$ is connected to the DE vertex $d$.
%IT contains a vertex labeled $S(\bar{d})$ for each symbol $S$ and tuple $\bar{d} \in S^{\struct}$.
%Two IT vertices labeled $S(d_1,\ldots,d_n)$ and $R(e_1,\ldots,e_n)$ have the same color iff $S=R$ and $d_i=e_i$ for all $i$ such that $\argpos{S}{i} \not \in A$.
% \jo{alternatively: $\bar{d}=\bar{e}$ except for the $i$th domain elements in the tuples where $\argpos{P}{i} \in A$.}
%An IT vertex labeled $S(\ldots,d_i,\ldots)$ is connected to the AP vertex $\argnode{d}{i}$ iff $\argpos{S}{i} \in A$.

The intuition behind the domain permutation graph $\dpg{\struct}{A}$ is that a permutation of its $DE$ vertices corresponds to a domain permutation $\pi$, a permutation of its $IT$ vertices corresponds to a permutation of domain element tuples in interpretations in $\struct$, and the $AP$ vertices and vertex coloring serve to link $DE$ and $IT$ in such a way that Definition~\ref{def:inducedstructuretransformation} is preserved for automorphisms.% of $\dpg{\struct}{A}$.
%More formally:

\begin{theorem}
\label{thm:domainpermutationgraph}
Let $\struct$ be a $\voc$-structure with domain $\domain$ and $A$ a set of argument positions.
% 
There exists a bijection between the automorphism group of the domain permutation graph \dpg{\struct}{A} and the group of domain permutations $\pi$ such that $\locdomsym{\pi}{A}(\struct)=\struct$. This bijection maps an automorphism $\tau$ to domain permutation $\pi$ iff $\tau(d)=\pi(d)$ for all $DE$ vertices (equated with domain elements) $d$.
\end{theorem}
% We refer to Appendix~\ref{prf:domainpermutationgraph} for a proof.

%As a result, given a model expansion problem $MX(\theory,\structin)$, decomposition $MX(\theorystar,\structinstar)$ and argument position set $A$ connectively closed under $\theorystar$, detecting automorphisms of the domain permutation graph $\dpg{\structinstar}{A}$ allows us to derive domain permutations $\pi$ such that $\locdomsym{\pi}{A}(\structinstar)=\structinstar$.

\begin{example}[Example~\ref{ex:graph_coloring_7} continued]
Using argument position set $A=\{\argpos{Edge_1}{1},\argpos{Edge_1}{2},\argpos{Color}{1},\argpos{x_1}{0},\argpos{y_1}{0},\argpos{x_3}{0}\}$ (which is connectively closed under $\theorygcstar$) and input structure $\structgcinstar$, the domain permutation graph \dpg{\structgcinstar}{A} is illustrated in Figure~\ref{fig:gcdpg}.
The automorphism group of \dpg{\structgcinstar}{A} corresponds to the group of induced structure transformations $\locdomsym{\pi}{A}$ such that $\locdomsym{\pi}{A}(\structgcinstar)=\structgcinstar$.
As a result, its automorphism group corresponds to a local domain symmetry group of $MX(\theorygc,\structgcin)$.
E.g., $\locdomsym{(t~u~v~w)}{A}$ corresponds to an automorphism that permutes the four left-most groups of five vertices, and $\locdomsym{(b~g)}{A}$ to an automorphism that swaps the two right-most groups of four vertices.
%\maurice{ als ik het goed begrijp: (t,u,v,w), (V(t),V(u),V(v),V(w)),
%  (EdgeZ(t,u),Edge(u,v),Edge(v,w),Edge(w,t)),
%  ([t,1],[u,1],[v,1],[w,1]), ([t,2],[u,2],[v,2],[w,2]) is een
%  automorphisme en het stuk ervan dat de domain permutatie beschrijft,
%  i.e. $\pi =(t,u,v,w)$ is een permutatie zo dat $\sigma_\pi^A$ een
%  local domain symmetry is. Ook, triviaal dat het automorphisme, een
%  permutatie is van de domein elementen (nodes van dezelfde color), de
%  input invariant laat (voor elke inputrelatie nodes van dezelfde
%  color) ; minder triviaal is wat het doet met de
%  argument posities (het ``verband'' tussen doman elementen en input
%  nodes wordt bewaard)}
\demo\end{example}

\begin{figure}
\label{fig:gcdpg}
\begin{tikzpicture}[-,scale=0.9]
  \tikzstyle{denode}=[regular polygon,regular polygon sides=3,minimum size=22pt,inner sep=0pt,draw]
  \tikzstyle{argnode0}=[diamond,minimum size=22pt,inner sep=0pt,draw]
  \tikzstyle{argnode1}=[circle,minimum size=22pt,inner sep=0pt,draw]
  \tikzstyle{argnode2}=[regular polygon,minimum size=22pt,inner sep=0pt,draw]
  \tikzstyle{tuplenode1}=[rectangle,minimum width=55pt, minimum height=20pt,inner sep=0pt,draw]
  \tikzstyle{tuplenode2}=[diamond,minimum size=22pt,inner sep=0pt,draw]

  \foreach \name/\x in {t/0, u/2.5, v/5, w/7.5}
    \node[denode] (\name) at (\x-1,-1) {$\name$};
  \foreach \name/\x in {t/0, u/2.5, v/5, w/7.5}
    \node[argnode2] (\name2) at (\x-1,-2) {$\argnode{\name}{2}$};
  \foreach \name/\x in {t/1, u/3.5, v/6, w/8.5}
    \node[argnode1] (\name1) at (\x-1,-2) {$\argnode{\name}{1}$};
  \foreach \name/\x in {t/1, u/3.5, v/6, w/8.5}
    \node[argnode0] (\name0) at (\x-1,-1) {$\argnode{\name}{0}$};

  \foreach \name/\x/\y in {r/10/-1, g/12/-1, b/11/-3}
    \node[denode] (\name) at (\x-1.3,\y) {$\name$};
  \foreach \name/\x/\y in {r/9/-2, g/11/-2, b/10/-4}
    \node[argnode2] (\name2) at (\x-0.3,\y) {$\argnode{\name}{2}$};
  \foreach \name/\x/\y in {r/10/-2, g/12/-2, b/11/-4}
    \node[argnode1] (\name1) at (\x-0.3,\y) {$\argnode{\name}{1}$};
  \foreach \name/\x/\y in {r/10/-1, g/12/-1, b/11/-3}
    \node[argnode0] (\name0) at (\x-0.3,\y) {$\argnode{\name}{0}$};

  \foreach \pred/\arg/\argg/\x in {Edge/t/u/0, Edge/u/v/2.5, Edge/v/w/5, Edge/w/t/7.5}
    \node[tuplenode1] (\pred\arg\argg) at (\x-0.5,-4) {$\pred_1(\arg,\argg)$};
%  \foreach \pred/\arg/\x in {V/t/0, V/u/3, V/v/6, V/w/9}
%    \node[tuplenode2] (\pred\arg) at (\x-0.5,0) {$\pred(\arg)$};

  \foreach \from/\to in {t/0,t/1,t/2,u/0,u/1,u/2,v/0,v/1,v/2,w/0,w/1,w/2,r/0,r/1,r/2,g/0,g/1,g/2,b/0,b/1,b/2}
    \draw (\from) -- (\from\to);

  \foreach \from/\to in {t1/tu,t2/wt,u1/uv,u2/tu,v1/vw,v2/uv,w1/wt,w2/vw}
    { \draw (\from) -- (Edge\to);}
%  \foreach \from/\to in {t1/t,u1/u,v1/v,w1/w}
%    { \draw (\from) -- (V\to);}
\end{tikzpicture}
\caption{Domain permutation graph \dpg{\structgcinstar}{A} with $A=\{\argpos{Edge_1}{1},\argpos{Edge_1}{2},\argpos{Color}{1},\argpos{x_1}{0},\argpos{y_1}{0},\argpos{x_3}{0}\}$. Each shape denotes a unique color, so vertices with the same shape have the same color.}
\end{figure}

Let $k$ be the largest arity of a symbol in $\struct$ for a domain permutation graph $\dpg{\struct}{A}$.
The size of $DE$ is $|\domain|$, the size of $AP$ is $(k+1)|\domain|$, and the size of $IT$ is $|\struct|$, which is $O(|\domain|^k)$.
Thus, the total number of nodes is $O(k|\domain|+|\domain|^k)$.
There are $(k+1)|\domain|$ edges between $DE$ and $AP$, and, if all argument positions over some symbol $S/k$ occur in $A$, then there are $O(k|\struct|)=O(k|\domain|^k)$ edges between $AP$ and $IT$.
Thus, the total number of edges is $O(k|\domain|^k)$.

Note that the size of \dpg{\struct}{A} does not depend on the size of the theory of the model expansion problem.
This is a major advantage compared to automorphism-based symmetry detection on ground theories, as the detection graph grows linearly with the ground theory~\cite{drtiwa11a}, which is typically much larger than the input structure.

\subsection{Subdomain interchangeability detection}
\label{sec:subd_intch_detection}
While the previous subsection detects local domain symmetry generators individually, it is not clear what type of symmetry group they form.
To optimally construct symmetry breaking constraints for symmetry groups, we need to detect subdomain interchangeability as well.
Then, by Theorem~\ref{thm:complete_breaking}, we will often be able to break subdomain interchangeability groups completely with a set of lex-leader constraints linear in $|\domain|$.

Given a model expansion problem $MX(\theory,\structin)$ with decomposition $MX(\theorystar,\structinstar)$ and a set of argument positions $A$ connectively closed under $\theorystar$, 
the task at hand is to find a subdomain $\delta \subseteq \domain$ such that for each permutation $\pi$ over $\delta$, $\locdomsym{\pi}{A}(\structinstar)=\structinstar$.
If so, Theorem~\ref{thm:locdomsymconditionmx} guarantees $\locdomsym{\pi}{A}$ to be a symmetry of $MX(\theory,\structin)$, which makes $\delta$ an $A$-interchangeable subdomain.

The actual algorithm finds a partition $\Delta$ of \domain, such that each $\delta \in \Delta$ is $A$-interchangeable.
The idea is based on the fact that the permutation group of a set is generated by swaps of two elements of the set.
As such, if we know for each pair $d_1,d_2 \in \domain$ whether $\locdomsym{(d_1~d_2)}{A}(\structinstar)=\structinstar$, it is straightforward to construct the partition $\Delta$.
The resulting symmetry detection algorithm is simple: for each pair of domain elements $d_1,d_2 \in \domain$, check whether $\locdomsym{(d_1~d_2)}{A}(\structinstar)=\structinstar$.
When using a disjoint-set data structure\footref{note:disjointset} to keep track of the partition $\Delta$, the complexity of this algorithm is $O(|\domain|^2|\structinstar|)$.
%When using a disjoint-set data structure \cite{disjoint_set} to keep track of the partition $\Delta$, the complexity of this algorithm is $O(|\domain|^2|\structinstar|)$.
The algorithm can be optimized by exploiting transitivity, domain element occurrence counting or unary symbols partitioning the domain, but this does not improve the worst-case complexity.

\begin{example}[Example~\ref{ex:graph_coloring_7} continued]
Given argument position set $A=\{\argpos{C_1}{1},\argpos{Color}{0}\}$ (which is connectively closed under $\theorygcstar$), we detect $A$-interchangeable domains by checking whether the (only) input symbol $C_1$ has the same interpretation in $\locdomsym{(d_1~d_2)}{A}(\structinstar)$ as in $\structinstar$ for combinations of $d_1,d_2 \in \{t,u,v,w,r,g,b\}$.
For $(d_1,d_2) \in \{(t,u),(u,v),(v,w),(r,g),(g,b)\}$ this is indeed the case. For $(d_1,d_2)=(w,r)$ this is not the case, so the $A$-interchangeable sets are $\{t,u,v,w\}$ and $\{r,g,b\}$.
\demo\end{example}

%\jo{mention preprocessing optimizations (calculate definitions, push quantifiers)?}
