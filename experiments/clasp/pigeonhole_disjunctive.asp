% Theory
1 <= {inhole(P,H) : hole(P) } :- pigeon(H).
:- inhole(P1,H), inhole(P2,H), P1!=P2.

% Data
pigeon(1..12).
hole(1..11).

% Display
#show inhole/2.
