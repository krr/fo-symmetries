package kodkod.examples.csp;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import kodkod.ast.Formula;
import kodkod.ast.Relation;
import kodkod.engine.Solution;
import kodkod.engine.Solver;
import kodkod.engine.config.ConsoleReporter;
import kodkod.engine.satlab.SATFactory;
import kodkod.instance.Bounds;
import kodkod.instance.TupleSet;
import kodkod.instance.Universe;
import kodkod.instance.TupleFactory;

// compile: <in kodkod>	waf configure --prefix=. --libdir=lib build install
// run: <in build> java -cp kodkod.jar:examples.jar -Djava.library.path=/home/jodv/workspace/kodkod/linux_x86_64 kodkod.examples.csp.PigeonHole 13 12 9999 0

public class PigeonHole  {
	private static Relation[] pigholes;
	private static Relation[] redundant;
	private static int symbreakingSize;
	private static boolean distractSymDetection;
	private static int pigeons;
	private static int holes;

	private Bounds bounds;

	public PigeonHole() {
    // Create domain
		final List<Object> atoms = new ArrayList<Object>(holes);
		for(int i = 0 ; i < holes; i++) { atoms.add(new Hole(i)); }
		final Universe u = new Universe(atoms);
		final TupleFactory f = u.factory();
		bounds = new Bounds(u);
		
		// Define p unary inHole relations
		pigholes = new Relation[pigeons];   
		for(int i = 0; i < pigeons; i++) { 
			pigholes[i] = Relation.unary("pig"+i+"hole");
		}
		final TupleSet all = f.allOf(1);
		for(Relation r : pigholes) { 
			bounds.bound(r, all);
		}
		// Define p redundant constants
		redundant = new Relation[holes];   
		TupleSet smalltuples = f.noneOf(1);
		for(int i = 0; i < holes; i++) { 
   		smalltuples.add(f.tuple(1,i));
			redundant[i] = Relation.unary("r"+i);
			bounds.boundexactly(redundant[i], smalltuples);
		}		
	}
	
	private static final class Hole {
		final int value;
		Hole(int value) { this.value = value; }
		public String toString() { return "hole"+value; }
	}
	
	/**
	 * Returns a formula stating that all pigeons
	 * have one hole, and that no two
	 * pigeons have intersecting holes.
	 * If required, also posts a redundant constraint
	 */
	public Formula constraints() {
		final List<Formula> formulas = new ArrayList<Formula>(pigholes.length);
		for(Relation r : pigholes) { 
			formulas.add( r.one() );
		}
		if(distractSymDetection){
		  for(Relation r : redundant) { 
			  formulas.add( r.one() );
		  }
		}
		for(int i = 0; i < pigholes.length; i++) { 
			final Relation ph = pigholes[i];
			for(int j = i+1; j < pigholes.length; j++) { 
				formulas.add( ph.intersection(pigholes[j]).no() );
			}
		}
		return Formula.and(formulas);
	}

	public Bounds bounds() { 
		return bounds.unmodifiableView();
	}
	
	private static void usage() { 
		System.out.println("Usage: java examples.csp.PigeonHole <nrPigeons> <nrHoles> <size of sym breaking constraint> <distraction level>");
		System.exit(1);
	}
	

	public static void main(String[] args) {
		if (args.length!=4) usage();
		
		try {
		  pigeons = Integer.parseInt(args[0]);
		  holes = Integer.parseInt(args[1]);
		  symbreakingSize = Integer.parseInt(args[2]);
		  distractSymDetection = Integer.parseInt(args[3])>0;
			final PigeonHole model = new PigeonHole();
			final Solver solver = new Solver();
			solver.options().setSolver(SATFactory.MiniSat);
			solver.options().setSymmetryBreaking(symbreakingSize);
			solver.options().setReporter(new ConsoleReporter());
			final Formula f = model.constraints();
			final Bounds b = model.bounds();
			final Solution sol = solver.solve(f, b);
			System.out.println(sol.outcome());
			System.out.println(sol.stats());
			if (sol.instance()!=null)
			  for(int i=0; i<pigholes.length; i++){
				  System.out.println("pigeon"+i+": "+sol.instance().tuples(model.pigholes[i]));
				}

		} catch (NumberFormatException e) { usage(); }
	}
}

