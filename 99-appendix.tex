\section{Proofs}\label{app:proofs}
% \subsection{Proof for Theorem~\ref{thm:locdomsymcondition}}
% \label{prf:locdomsymcondition}

\noindent
\textit{Theorem~\ref{thm:locdomsymcondition}}\\
\noindent
Let \voc be a vocabulary, \theory a theory over $\voc$, $\pi$ a domain permutation and $A$ a set of argument positions. If $A$ is connectively closed under \theory, then $\locdomsym{\pi}{A}$ is a local
domain symmetry of \theory.
\begin{proof}
To prove this theorem, we prove the following consecutive claims for each \voc-structure $\struct$. 
Without loss of generalization, we assume \struct interprets the neccessary variables.
\begin{enumerate}
 \item For each term $f(\ttt)$ that occurs in \theory, it holds that $f(\ttt)^{\locdomsym{\pi}{A}(\struct)}=\left\{ \begin{array}{ll} \pi(f(\ttt)^\struct) & \text{ if $f|0 \in A$}\\
                                                                                              f(\ttt)^\struct &\text{ otherwise}
                                                                                             \end{array} \right.$
\item For each atom $a$ of the form $P(t_1,\ldots,t_n)$ or of the form $t_1=t_2$ that occurs in \theory, it holds that $a^{\locdomsym{\pi}{A}(\struct)} = a^\struct$. 
\item For each formula $\varphi$  that occurs in \theory,  $\varphi^{\locdomsym{\pi}{A}(\struct)} = \varphi^\struct$. 
% \pi_{\symb{t},0}(t^\struct) \]
\end{enumerate}
The first claim is proven by induction on the subterm relation. The induction step follows from the fact that $A$ is connectively closed. 
The second claim follows from the first, also using the fact that $A$ is connectively closed.
Consider for instance the case of atom $f(\ttt)=g(\ttt')$ occurring in \theory, with $f|0\in A$.
Then $g|0\in A$ (since $A$ is connectively closed), so $f(\ttt)^{\locdomsym{\pi}{A}(\struct)}=g(\ttt')^{\locdomsym{\pi}{A}(\struct)}$ iff $\pi(f(\ttt)^\struct)=\pi(g(\ttt')^\struct)$ iff $f(\ttt)^\struct=g(\ttt')^\struct$ (since $\pi$ is a permutation). 
The other cases are analogous.

The last claim follows by induction on the subformula relation since the value of a first-order formula is entirely determined by the value of the atoms occuring in it.
Consider for instance the case of formula $\exists x\colon \varphi$ occurring in \theory with $\argpos{x}{0} \in A$.
$(\exists x\colon \varphi)^\struct$ holds iff there exists a $d \in \domain$ such that $\varphi^{\struct[x:d]}$ holds.
By the induction hypothesis, $\varphi^{\struct[x:d]}=\varphi^{\locdomsym{\pi}{A}(\struct[x:d])}=\varphi^{\locdomsym{\pi}{A}(\struct)[x:\pi(d)]}$ (since $\argpos{x}{0} \in A$).
$(\exists x\colon \varphi)^{\locdomsym{\pi}{A}(\struct)}$ holds iff there exists a $d' \in \domain$ such that $\varphi^{\locdomsym{\pi}{A}(\struct)[x:d']}$ holds.
Without loss of generalization, let $d'=\pi(d)$, then $(\exists x\colon \varphi)^\struct=(\exists x\colon \varphi)^{\locdomsym{\pi}{A}(\struct)}$.
The other cases are analogous.
\end{proof} 

% \subsection{Proof for Theorem~\ref{thm:locdomsymconditionmx}}
% \label{prf:locdomsymconditionmx}
\noindent
\textit{Theorem~\ref{thm:locdomsymconditionmx}}\\
\noindent
Let $MX(\theory,\structin)$ be a model expansion problem with decomposition $MX(\theorystar,\structinstar)$.
If $A$ is connectively closed under $\theorystar$ and $\locdomsym{\pi}{A}(\structinstar)=\structinstar$
then $\locdomsym{\pi}{A}$ is a symmetry for $MX(\theory,\structin)$.
\begin{proof}
Since $MX(\theory,\structin)$ and $MX(\theorystar,\structinstar)$ have the same set of solutions, it suffices to prove that $\locdomsym{\pi}{A}$ is a symmetry of $MX(\theorystar,\structinstar)$.
Firstly, due to the connectively closed condition, $\locdomsym{\pi}{A}$ is a symmetry of $\theorystar$, so $\structinstar \sqcup \structout \models \theorystar$ iff $\locdomsym{\pi}{A}(\structinstar \sqcup \structout) \models \theorystar$. 
Secondly, since $\locdomsym{\pi}{A}(\structinstar)=\structinstar$, $\structinstar \sqcup \structout \models \theorystar$ iff $\structinstar \sqcup \locdomsym{\pi}{A}(\structout) \models \theorystar$, so $\locdomsym{\pi}{A}$ is a symmetry for $MX(\theorystar,\structinstar)$.
\end{proof}

% \subsection{Proof for Theorem~\ref{thm:complete_breaking}}
% \label{prf:complete_breaking}
\noindent
\textit{Theorem~\ref{thm:complete_breaking}}\\
\noindent
Let $MX(\theory,\structin)$ be a model expansion problem, $\delta$
an $A$-interchangeable subdomain,
$\preceq_\domain$ a total order on domain \domain and $s(d)$ the successor of $d$ in $\delta$ according to $\preceq_\domain$.
If $A$ contains at most one argument position $\argpos{S}{i}$ for each symbol $S \in \vocout$, then the conjunction of lex-leader constraints
\[ \{ \lex{\locdomsym{(d~s(d))}{A}} \mid d\in \delta \} \]
is a complete symmetry breaking constraint for the subdomain
interchangeability group $\locdomintch{\delta}{A}$.
\begin{proof}
To prove this theorem, we (1) convert the task of finding a solution to a model expansion problem to a constraint programming problem, where an assignment over a set of Boolean variables $V$ has to be found.
Further, we show that (2) a subset of these Boolean variables can be organized as a matrix $M_\delta$, where each permutation over the rows of $M_\delta$ corresponds to a permutation over $\delta$.
The interchangeability group $\locdomintch{\delta}{A}$ then corresponds to a row interchangeability symmetry group induced by permuting $M_\delta$'s rows.
Using a result from constraint programming, such row interchangeability symmetry groups are broken completely by posting a lex-leader constraint (based on the appropriate row ordering) for each symmetry induced by the swap of two consecutive rows~\cite{row_column_sym_csp,cspsat/DevriendtBB14}. This corresponds to posting $\{ \lex{\locdomsym{(d~s(d))}{A}} \mid d\in \delta \}$, ending the proof.

(1) Given a vocabulary \vocout and a domain \domain, finding a $\vocout$-structure consists of deciding for each $\bar{d} \in D^n$ whether $\bar{d} \in S^{\structout}$ for each symbol $S/n \in \vocout$.
Hence, a model expansion problem $MX(\theory,\structin)$ can be seen as finding an assignment to a set of Boolean variables $V=\{S(\bar{d}) \mid S/n \in \vocout, \bar{d} \in D^n\}$ such that $\structin \sqcup \structout \models \theory$.
A local domain symmetry $\locdomsym{\pi}{A}$ for $MX(\theory,\structin)$ now corresponds to a \emph{variable symmetry}~\cite{row_column_sym_csp} mapping 
\[ S(d_1,\ldots,d_n) ~~~ \text{ to } ~~~ S(\localperm{}{S}{1}(d_1),\ldots, \localperm{}{S}{n}(d_n)) \]
where $\localperm{}{S}{i}(d) = \pi(d)$ if $\argpos{S}{i} \in A$ and $\localperm{}{S}{i}(d) =d$ otherwise.

(2) The variables in $V$ that are not fixed by some $\locdomsym{\pi}{A} \in \locdomintch{\delta}{A}$ are those $S(\ldots,d_{j-1},\delta_i,d_{j+1},\ldots)$ where $\delta_i \in \delta$ is the $j$th domain element of an $S$-tuple with $\argpos{S}{j} \in A$.
We can partition this subset into ``rows'' $R_{\delta_i} = \{S(\ldots,d_{j-1},\delta_i,d_{j+1},\ldots) \mid \argpos{S}{j} \in A, d_k \in \domain\}$ where $\delta_i$ is fixed.
It is clear that $\locdomsym{\pi}{A}(R_{\delta_i})=R_{\pi(\delta_i)}$, so a permutation of the set of rows corresponds to a symmetry of $\locdomintch{\delta}{A}$.
Since $A$ contains at most one argument position for each $S \in \vocout$, these rows are pairwise disjoint, and under some column organization form the requested matrix $M_\delta$.
\end{proof}

\newcommand\dA[1]{#1_{A}^+}
\newcommand\dnotA[1]{#1_{A}^-}
% \subsection{Proof for Theorem~\ref{thm:domainpermutationgraph}}
% \label{prf:domainpermutationgraph}
\noindent
\textit{Theorem~\ref{thm:domainpermutationgraph}}\\
\noindent
Let $\struct$ be a $\voc$-structure with domain $\domain$ and $A$ a set of argument positions.

There exists a bijection between the automorphism group of the domain permutation graph \dpg{\struct}{A} and the group of domain permutations $\pi$ such that $\locdomsym{\pi}{A}(\struct)=\struct$. This bijection maps an automorphism $\tau$ to domain permutation $\pi$ iff $\tau(d)=\pi(d)$ for all $DE$ vertices (equated with domain elements) $d$.

\begin{proof}
We prove the bijection by showing that all induced structure transformations $\locdomsym{\pi}{A}$ with $\locdomsym{\pi}{A}(\structin)=\structin$ correspond to an automorphism of \dpg{\structin}{A} ($\Rightarrow$) and vice versa ($\Leftarrow$).

First, some preliminaries. For a given symbol $S$, let each tuple $(d_1,\ldots,d_n) \in S^\structin$ be split as two tuples $\dA{d}\dnotA{d}$ such that $\dA{d}=\{d_i \mid \argpos{S}{i} \in A\}$ and $\dnotA{d}=\{d_i \mid \argpos{S}{i} \not \in A\}$.
Let $\pi$ naturally extend to tuples: $\pi((d_1,\ldots,d_n))=(\pi(d_1),\ldots,\pi(d_n))$.
The symmetrical interpretation $\locdomsym{\pi}{A}(\structin)$ can then be described as $\{\pi(\dA{d})\dnotA{d} \mid \dA{d}\dnotA{d} \in S^\structin\}$, so $\locdomsym{\pi}{A}(\structin)=\structin$ iff for all symbols $S$, $\dA{d}\dnotA{d} \in S^\structin$ iff $\pi(\dA{d})\dnotA{d} \in S^\structin$.
Also, without loss of generalization, let an IT vertex's label be $S(\dA{d}\dnotA{d})$ for symbol $S$.
Lastly, $(v,w) \in E$ denotes that graph $E$ has an (undirected) edge between vertices $v$ and $w$.

%Now, an automorphism of \dpg{\structin}{A} can be characterized as a set of permutations $DE\colon DEV \to DEV$, $AP(k)\colon APV \to APV$ where $1 \leq k \leq \text{maximum arity of symbols in \structin}$, $IT(S,\dnotA{d}) \colon IT \to IT$. \jo{explain permutations}

($\Rightarrow$)
If $\locdomsym{\pi}{A}(\structin)=\structin$, 
$\locdomsym{\pi}{A}$ corresponds to a permutation $\alpha$ of the vertices of \dpg{\structin}{A}: $\alpha(d)=\pi(d)$ (for DE vertices), $\alpha(\argnode{d}{i})=\argnode{\pi(d)}{i}$ (for AP vertices), $\alpha(S(\dA{d}\dnotA{d}))=S(\pi(\dA{d})\dnotA{d})$ (for IT vertices).
We show that $\alpha$ is an automorphism of \dpg{\structin}{A}.

By the definition of \dpg{\structin}{A}, $\alpha$ preserves the colors.
To show that $\alpha$ preserves the edges, we need to show that 
$(v,w) \in \dpg{\structin}{A}$ iff
$(v,w) \in \alpha(\dpg{\structin}{A})$.
Firstly, remark that $\alpha$ maps each vertex in a layer to another vertex in that layer, so we only need to check whether the edges between (1) DE-AP and (2) AP-IT are conserved.

(1) The following statements are equivalent
\begin{align*}
(\alpha(d),\alpha(\argnode{e}{i})) \in \alpha(\dpg{\structin}{A}) & \\
(d,\argnode{e}{i}) \in \dpg{\structin}{A} &\text{ ($\alpha$ is a permutation of vertices) } \\
d=e &\text{ (definition of domain permutation graph)  } \\
\pi(d)=\pi(e) &\text{ ($\pi$ is a permutation)  } \\
(\pi(d),\argnode{\pi(e)}{i}) \in \dpg{\structin}{A} &\text{ (definition of domain permutation graph) } \\
(\alpha(d),\alpha(\argnode{e}{i})) \in \dpg{\structin}{A} &\text{ (definition of $\alpha$) }
\end{align*}

(2) Similarly, the following statements are equivalent
\begin{align*}
(\alpha(\argnode{d}{i}),\alpha(S(\dA{d}\dnotA{d})) \in \alpha(\dpg{\structin}{A}) & \\
(\argnode{d}{i},S(\dA{d}\dnotA{d})) \in \dpg{\structin}{A} &\text{ ($\alpha$ is a permutation of vertices) } \\
d_i \in \dA{d} &\text{ (definition of domain permutation graph)  } \\
\pi(d_i) \in \pi(\dA{d}) &\text{ ($\pi$ is a permutation)  } \\
(\argnode{\pi(d)}{i},S(\pi(\dA{d})\dnotA{d})) \in \dpg{\structin}{A} &\text{ (definition of domain permutation graph) } \\
(\alpha(\argnode{d}{i}),\alpha(S(\dA{d}\dnotA{d})) \in \dpg{\structin}{A} &\text{ (definition of $\alpha$) }
\end{align*}

($\Leftarrow$)
We must show that an automorphism $\alpha$ of $\dpg{\structin}{A}$ corresponds to an $A,\pi$-induced structure transformation $\locdomsym{\pi}{A}$ such that $\locdomsym{\pi}{A}(\structin)=\structin$.

Notice that, since $\alpha$ is an automorphism of a three-layered graph with different colors for each layer DE, AP and IT, we can write it as a composition of three permutations $\alpha_{DE} \circ \alpha_{AP} \circ \alpha_{IT}$.
As there exists a bijection between DE and the domain \domain of \structin, we assume $\alpha_{DE}\backsimeq\pi$, with $\pi$ a permutation of \domain.
%As there exists a bijection between IT and the tuples in interpretations in \structin, we assume $\alpha{IT}=\sigma$, with $\sigma$ a transformation of $\structin$.

We now show that
(1) $\alpha(\argnode{d}{i})=\argnode{\pi(d)}{i}$ and
(2) $\alpha(S(\dA{d}\dnotA{d}))=S(\pi(\dA{d})\dnotA{d})$.
From this, it follows that $\alpha$ represents a structure transformation $\locdomsym{\pi}{A}$ mapping tuples $\dA{d}\dnotA{d}$ to $\pi(\dA{d})\dnotA{d}$, and hence, $\locdomsym{\pi}{A}(\structin)=\structin$.

(1) Since $\argnode{d}{i}$ and $\argnode{e}{j}$ have the same color iff $i=j$, $\alpha(\argnode{d}{i})=\argnode{e}{i}$ for some domain element $e$. As each vertex $\argnode{d}{i}$ is connected to exactly one vertex $d$, $\alpha(\argnode{d}{i})=\argnode{\pi(d)}{i}$.

(2) Since $S(\dA{d}\dnotA{d})$ and $R(\dA{e}\dnotA{e})$ have the same color iff $S=R$ and $\dnotA{d}=\dnotA{e}$, $\alpha(S(\dA{d}\dnotA{d}))=S(\dA{e}\dnotA{d})$ for some tuple domain elements $e$. 
All that is left to show is that $\dA{e}=\pi(\dA{d})$.
For this, note that $S(\dA{d}\dnotA{d})$ is connected only to $\argnode{d}{i}$ for each $d$ on index $i$ in $\dA{d}$. 
As $\alpha$ is an automorphism that maps $\argnode{d}{i}$ to $\argnode{\pi(d)}{i}$, $\alpha(S(\dA{d}\dnotA{d})$ must be connected only to all $\argnode{\pi(d)}{i}$.
The only vertex doing so (taking colors into account) is $S(\pi(\dA{d})\dnotA{d})$.
\end{proof}

%\jo{For this type of proof, the \sbass paper proves that automorphism groups of the graph and of the logic program are isomorphic. They do this by constructing a bijective homomorphism.
%I only gave a bijection proof. Does this suffice? Is such a bijection $f$ also a homomorphism if they preserve the group structure (e.g., $a \circ b = c$ iff $f(a) \circ f(b) = f(c)$?). Why is this needed?
%}
