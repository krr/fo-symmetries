%\jo{$\varphi$ vs $\varphi$, node vs vertex}

A standard approach of dealing with symmetry extends the theory with
\emph{symmetry breaking constraints} that eliminate symmetric
solutions while guaranteeing that at least one solution to the
original problem is preserved (if it exists). This way, a search
algorithm will not get stuck in parts of the search space symmetric to those already explored. 
% 
A set of symmetry breaking constraints $\varphi$ is {\em sound} for a symmetry group $\mathbb{G}$ if for each solution $\struct$, there exists \emph{at least} one $\sigma \in \mathbb{G}$ such that $\sigma(\struct)$ satisfies $\varphi$; it is \emph{complete}
if there exists \emph{at most} one such $\sigma \in \mathbb{G}$~\cite{walsh_recent_results_2012}.

Often, symmetry breaking is done by defining a lexicographical order over the set of 
candidate solutions. For a given symmetry, so-called \emph{lex-leader constraints} then encode that each solution's symmetrical image cannot be strictly smaller under the defined lexicographical order.
As long as the chosen lexicographical order is fixed, the conjunction of lex-leader constraints for any set of symmetries is sound.

In a model expansion context, the set of candidate solutions $\Gamma_\domain^\vocout$ consists of all $\vocout$-structures with domain \domain. A logical formula that is added to the theory takes over the role of a constraint.
We construct a lexicographical order $\preceq_\Gamma$ over $\Gamma_\domain^\vocout$ from an order $\preceq_\domain$ over \domain and an order $\preceq_{\vocout}$ over \vocout.
Then, $\structout \prec_\Gamma \structout'$ iff there exists some symbol $S \in \vocout$ and domain element tuple $\bar{d}$ such that $\bar{d} \not \in S^{\structout}$, $\bar{d} \in S^{\structout'}$, and for all $\bar{d}' \prec_\domain \bar{d}$ it holds that $\bar{d}' \in S^{\structout} \Leftrightarrow \bar{d}' \in S^{\structout'}$, and for all $S' \prec_{\vocout} S$, it holds that $S'^{\structout}=S'^{\structout'}$.
%
%A lexicographical order over $\Gamma_\domain^\vocout$ is straightforward to construct from an order over \domain and an order over \vocout.
For the remainder of this section, we leave the order over $\vocout$ implicit, but explicitly state the order $\preceq_\domain$ over \domain, as this turns out to be important.
Given a model expansion problem with symmetry $\sigma$ and a lexicographical order over $\Gamma_\domain^\vocout$ with $\preceq_\domain$ as \domain-order, we use $\lex{\sigma}$ to refer to the logical formula encoding the lex-leader constraint for $\sigma$. Efficient encodings of the lex-leader constraint into formulas are well-known~\cite{Sak09HBSAT}.

\begin{example}[Example~\ref{ex:graph_coloring_start} continued]
Let $t \prec_\domain u \prec_\domain v \prec_\domain w \prec_\domain r \prec_\domain g \prec_\domain b$ and $A=\{\argpos{C}{1},\argpos{Color}{0}\}$.
For $MX(\theorygc,\structgcin)$, 
the local domain symmetry $\locdomsym{(r~g)}{A}$  is broken by the lex-leader constraint $\lex{\locdomsym{(r~g)}{A}}$, which informally implies that for each vertex $v$, if all vertices $v' \prec_{\domain} v$ are not colored by $r$ or by $g$, then $v$ cannot be colored with $r$. 
Amongst others, this constraint cuts away $\vocout$-structures %$\structgcout$ 
that color $t$ with $r$.
%, as $(t,r) \in Color^\structgcout$ implies that $\structgcout$ is lexicographically smaller than $\locdomsym{(r~g)}{\{\argpos{C}{1},\argpos{Color}{0}\}}(\structgcout)$.
\demo\end{example}

Note that lex-leader constraints are constructed for individual
symmetries. In general, to obtain a complete symmetry breaking
constraint for a symmetry group $\mathbb{G}$, one needs to post
$\lex{\sigma}$ for each $\sigma \in \mathbb{G}$. As symmetry groups can contain a factorial amount of symmetries this is infeasible, e.g., in the case of subdomain interchangeability.
Instead, the standard approach is \emph{partial symmetry breaking}, where
$\lex{\sigma}$ is posted for a minimal set of generators $\sigma$ of
$\mathbb{G}$~\cite{Shatter}. 
Partial symmetry breaking is feasible, but does not guarantee that $\mathbb{G}$ is broken completely, leaving symmetrical parts of the search space open
to a search engine.

For instance, for a subdomain interchangeability group $\locdomintch{\delta}{A}$, a minimal set of generator symmetries is $\{\locdomsym{(d~s(d))}{A} \mid d, s(d) \in \delta\}$, where $s(d)$ is the successor of $d$ in $\delta$ according to $\preceq_\domain$.
Other minimal generator sets exist as well, e.g., $\{\locdomsym{(d_0~d)}{A} \mid d \in \delta, d\neq d_0\}$ for a fixed $d_0 \in \delta$.
%Partial symmetry breaking constructs a symmetry breaking constraint for each symmetry in such a generator set.
However, the choice of the generator set influences the power of the symmetry breaking formula.
For subdomain interchangeability groups $\mathbb{G}$, choosing the right generator set can guarantee that the lex-leader constraints used in partial symmetry breaking are actually complete for $\mathbb{G}$:

\begin{theorem}
\label{thm:complete_breaking}
Let $MX(\theory,\structin)$ be a model expansion problem, $\delta$
an $A$-interchangeable subdomain,
$\preceq_\domain$ a total order on domain \domain and $s(d)$ the successor of $d$ in $\delta$ according to $\preceq_\domain$.
If $A$ contains at most one argument position $\argpos{S}{i}$ for each symbol $S \in \vocout$, then the conjunction of lex-leader constraints
\[ \{ \lex{\locdomsym{(d~s(d))}{A}} \mid d, s(d) \in \delta \} \]
is a complete symmetry breaking constraint for the subdomain
interchangeability group $\locdomintch{\delta}{A}$.
\end{theorem}
% We refer to Appendix~\ref{prf:complete_breaking} for a proof.

A strongly related result is that when constructing a relation $R \subseteq \domain_1 \times \ldots \times \domain_n$ for which exactly one dimension $D_i$ contains interchangeable values, an efficient lex-leader constraint exists that completely breaks the resulting symmetry~\cite{dam/shlyakter2007}.
Theorem~\ref{thm:complete_breaking} can be seen as a conversion of this result to a model expansion context with local domain interchangeability.

Intuitively, Theorem~\ref{thm:complete_breaking} states that local
domain interchangeability is completely broken by a linear number of lex-leader
constraints if the set of argument positions contains at most one
argument position for each output symbol. These lex-leader constraints
$\lex{\locdomsym{(d~s(d))}{A}}$ are based on swaps $(d~s(d))$
of two consecutive domain elements over the chosen domain
ordering. Note that lex-leader constraints based on swaps of
non-consecutive domain elements, e.g., $\{\locdomsym{(d_0~d)}{A} \mid d \in \delta, d\neq d_0\}$ for a fixed $d_0$, do not have this property~\cite{cspsat/DevriendtBB14}.

\begin{example}[Example~\ref{ex:graph_coloring_start} continued]
Given the graph coloring problem $MX(\theorygc,\structgcin)$, let $A=\{\argpos{C}{1},\argpos{Color}{0}\}$ and $r \prec_\domain g \prec_\domain b$. $\locdomintch{\{r,g,b\}}{A}$ is a subdomain interchangeability group of $MX(\theorygc,\structgcin)$. 
Since $A$ contains only one argument position for the symbol $Color$, it is completely broken by 
\begin{flalign*}
 \qquad\qquad\qquad\qquad\qquad\qquad\quad&\lex{\locdomsym{(r~g)}{A}} \land \lex{\locdomsym{(g~b)}{A}}&\demo 
 \end{flalign*}
%but not by 
%\[ \lex{\locdomsym{(r~g)}{A}} \land \lex{\locdomsym{(r~b)}{A}} \]
\end{example}

The finite model generation system \SEM{} also breaks this type of symmetry completely, by way of \emph{dynamically} avoiding symmetrical decisions during search.~\cite{Zhang95sem}
The more recent model generator \kodkod{}~\cite{tacas/TorlakJ07} breaks symmetry statically by posting lex-leader constraints from~\citeN{Shatter} for global domain symmetry.
Although~\citeN{tacas/TorlakJ07} do not mention any completeness result, experiments with a pigeonhole encoding in \kodkod{} indicate that it uses the right set of generator symmetries to completely break all pigeon and hole interchangeability symmetry.
