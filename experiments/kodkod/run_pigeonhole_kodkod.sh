#!/bin/sh
cd ~/workspace/kodkod_src/kodkod

##wget --no-check-certificate https://waf.io/waf-1.8.12
##chmod u+x waf-1.8.12

alias waf=$PWD/waf-1.8.12
export JAVA_HOME="/usr/lib/jvm/jdk1.8.0/"
waf configure --prefix=. --libdir=lib build install

cd build
java -cp kodkod.jar:examples.jar -Djava.library.path=/home/jodv/workspace/kodkod/linux_x86_64 kodkod.examples.csp.PigeonHole 20 19 9999999 1
