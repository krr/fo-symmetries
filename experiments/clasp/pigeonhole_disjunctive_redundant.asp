% Theory
%redundant(X,Y) :- X<Y, pigeon(X), pigeon(Y).
1 <= {inhole(P,H) : hole(H) } :- pigeon(P).
:- inhole(P1,H), inhole(P2,H), P1!=P2.

% Data
pigeon(1..65).
hole(1..64).

% Display
#show inhole/2.
